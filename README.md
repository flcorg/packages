# Composer Packages for FLC projects

This repository provides static details for composer updates.

## Usage

Add the following configuration to the composer.json file of your project:

```
"repositories": [
    {"type": "composer", "url": "https://gitlab.com/flcorg/packages/"}
]
```

## Maintenance

Clone the repository and fetch dist files

```
$ git clone https://gitlab.com/flcorg/packages flc-packages
$ cd flc-packages
$ git lfs fetch origin master
```

